const llamarRazas = async () =>{
    url = "https://dog.ceo/api/breeds/list"
    
    axios
    .get(url)
    .then((res) =>
        cargarCombo(res.data)
    )
    .catch((err) =>{
        console.log("Ocurrio un error al llamar a las razas " + err)
    })
}

const llamarImagen = async (raza) =>{
    url = "https://dog.ceo/api/breed/" + raza + "/images/random"
    
    axios
    .get(url)
    .then((res) =>
        cargarImagen(res.data)
    )
    .catch((err) =>{
        alert("Ocurrio un error al cargar la foto " + err)
    })
}

const cargarImagen = (data) => {

    
    const img = document.getElementById('imgRaza')
    if (data.status === "success"){
        img.src = data.message
    }
    else{
        alert("Error al cargar imagen")
    }

}

const cargarCombo = (data) =>{
    const cmb = document.getElementById('cmbRaza')
    for (var i in data.message){
       cmb.innerHTML += '<option value"' + data.message[i] + '">'
       + data.message[i] + '</option>'
       //console.log(data.message[i])
    }
    alert("Razas Cargadas Existosamente")
    

}

document.getElementById('btnCargar').addEventListener('click', () =>{
    llamarRazas();
})

document.getElementById('btnImagen').addEventListener('click', () =>{
    const raza = document.getElementById('cmbRaza').value;
    llamarImagen(raza);
})


